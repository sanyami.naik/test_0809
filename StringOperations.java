package test_0809;

import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

class StringDemo
{
    void sortLengthwise(String string)
    {
        int i=0;
        StringTokenizer st=new StringTokenizer(string," ");
        String[] array=new String[4];
        while(st.hasMoreTokens())
        {
            String s=st.nextToken();
            array[i]=s;
            i++;
        }

        //System.out.println(Arrays.toString(array));

        String temp;
        for(int index=0;index<array.length-1;index++)
        {
            for(int innerIndex=index+1;innerIndex<array.length-1;innerIndex++)
            {
                if(array[index].length()> array[innerIndex].length())
                {
                    temp=array[index];
                    array[index]=array[innerIndex];
                    array[innerIndex]=temp;
                }

            }
        }

        System.out.println("The sentence in lengthwise sorting is");
        for(String str:array)
        {
            System.out.print(str+" ");
        }
        System.out.println();
    }



    void sortAlphabetically(String string)
    {
        int i=0;
        StringTokenizer st=new StringTokenizer(string," ");
        String[] array=new String[4];
        while(st.hasMoreTokens())
        {
            String s=st.nextToken();
            array[i]=s;
            i++;
        }

        //System.out.println(Arrays.toString(array));

        System.out.println("The sorted string with alphabets is");
        Arrays.sort(array);

        for(String str:array)
        {
            System.out.print(str+" ");
        }

        System.out.println();

    }



    void addDollar(String string)
    {

        int i=0;
        StringTokenizer st=new StringTokenizer(string," ");
        String[] array=new String[4];
        while(st.hasMoreTokens())
        {
            String s=st.nextToken();
            array[i]=s;
            i++;
        }



        Arrays.sort(array);
        System.out.println("The dollar added string is");
        for(String str:array)
        {
            System.out.print(str+" $ ");
        }

        System.out.println();
    }


    void reverseString(String string)
    {

        String neww="";
        for(int i=0;i<string.length()-1;i++)
        {
            char c=string.charAt(i);
            neww=c+neww;
        }

        System.out.println("The reverse string is");
        System.out.println(neww);
    }
}


public class StringOperations {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        char ch='Y';
        System.out.println("Enter the String you want to check");
        String string = sc.nextLine();


        StringDemo stringDemo=new StringDemo();


        do {
            System.out.println("Press 1 for sorting lengthwise");
            System.out.println("Press 2 for sorting Alphabetically");
            System.out.println("Press 3 for adding $");
            System.out.println("Press 4 for reversing");

            System.out.println("Enter your choice");
            int op = sc.nextInt();

            switch (op) {
                case 1:
                    stringDemo.sortLengthwise(string);
                    break;

                case 2:
                    stringDemo.sortAlphabetically(string);
                    break;


                case 3:
                    stringDemo.addDollar(string);
                    break;

                case 4:
                    stringDemo.reverseString(string);



            }

            System.out.println("Do you want to continue?Press y");
            ch=sc.next().charAt(0);
        } while (ch == 'Y' || ch == 'y');
    }
}





/*OUTPUT:
Enter the String you want to check
I Love My India
Press 1 for sorting lengthwise
Press 2 for sorting Alphabetically
Press 3 for adding $
Press 4 for reversing
Enter your choice
1
The sentence in lengthwise sorting is
I My Love India
Do you want to continue?Press y
y
Press 1 for sorting lengthwise
Press 2 for sorting Alphabetically
Press 3 for adding $
Press 4 for reversing
Enter your choice
2
The sorted string with alphabets is
I India Love My
Do you want to continue?Press y
y
Press 1 for sorting lengthwise
Press 2 for sorting Alphabetically
Press 3 for adding $
Press 4 for reversing
Enter your choice
3
The dollar added string is
I $ India $ Love $ My $
Do you want to continue?Press y
y
Press 1 for sorting lengthwise
Press 2 for sorting Alphabetically
Press 3 for adding $
Press 4 for reversing
Enter your choice
4
The reverse string is
idnI yM evoL I
Do you want to continue?Press y
n

 */